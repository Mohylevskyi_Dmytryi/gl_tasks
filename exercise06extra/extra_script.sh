#!/bin/bash
function disable_pin {
echo 26 > /sys/class/gpio/unexport
echo 16 > /sys/class/gpio/unexport
echo 20 > /sys/class/gpio/unexport
echo 21 > /sys/class/gpio/unexport
exit 1
}

mode=
trap 'echo "exit"; disable_pin' 2
echo 26 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio26/direction

echo 16 > /sys/class/gpio/export
echo out  > /sys/class/gpio/gpio16/direction

echo 20 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio20/direction

echo 21 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio21/direction


while [ true ]
do

var=`cat /sys/class/gpio/gpio26/value`
 
if [[ $var == "1" ]] ; then
mode=$(( mode ^ ( 1 << 0 ) ))
fi

if [[ $mode == "1" ]] ; then

echo 1 >  /sys/class/gpio/gpio16/value
sleep .5
echo 0 >  /sys/class/gpio/gpio16/value

echo 1 >  /sys/class/gpio/gpio20/value
sleep .5
echo 0 >  /sys/class/gpio/gpio20/value

echo 1 >  /sys/class/gpio/gpio21/value
sleep .5
echo 0 >  /sys/class/gpio/gpio21/value

elif  [[ $mode == "0" ]] ; then

echo 1 >  /sys/class/gpio/gpio21/value
sleep .5
echo 0 >  /sys/class/gpio/gpio21/value

echo 1 >  /sys/class/gpio/gpio20/value
sleep .5
echo 0 >  /sys/class/gpio/gpio20/value

echo 1 >  /sys/class/gpio/gpio16/value
sleep .5
echo 0 >  /sys/class/gpio/gpio16/value

fi
done
