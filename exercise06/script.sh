#!/bin/bash

function disable_pin {
echo 26 > /sys/class/gpio/unexport
exit 1
}

echo 26 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio26/direction
trap 'echo "exit"; disable_pin' 2
var2=true

while [ true ]
do 

var=`cat /sys/class/gpio/gpio26/value`

if [[ $var == "1" && $var2 == true  ]] ; then

count=$((count+1))
echo $count
var2=false

elif [[ $var == "0"  ]] ; then 

var2=true

fi
done 
