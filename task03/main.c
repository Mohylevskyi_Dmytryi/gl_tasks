
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h>
void RLE_encode(FILE *input,FILE *output)
{
    int last = 0;
    int c;
 
    while ( ( c = getc( input ) ) >= 0 )  
    {   
        putc( (char) c, output );
 
        if ( c == last ) 
        {
            int count = 0;
            while ( count < 255 && ( ( c = getc( input ) ) >= 0 ) ) 
            {
                if ( c == last )
                {
                    count++;
                }
                else
                {
                    break;
                }
            }
            putc( (char) count, output );
            if ( count != 255 && c >= 0 )
            {
                putc( (char) c, output );
            }
        }
        last = c;
    }
}
 
void RLE_decode(FILE *input,FILE *output)
{  
    int last = 0;
    int c;
    int count;
 
    while ( ( c = getc( input ) ) >= 0 )  
    {
        putc( (unsigned char) c, output );
 
        if ( c == last ) 
        {
            count = getc( input );
            while ( count-- > 0 )
            {
                putc( (char) c, output );
            }
        }
        last = c;
    }
}
 

int main ()
{
FILE *in;
FILE *out;
 in = fopen("in.txt", "r");
 out = fopen("out.txt","w"); 
RLE_encode(in,out);
fclose(in);
fclose(out);
in = fopen("out.txt", "r");
out = fopen("encod.txt","w"); 
RLE_decode(in,out);
fclose(in);
fclose(out);

    return 0;
}

