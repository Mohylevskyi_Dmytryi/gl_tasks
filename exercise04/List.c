#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct Node {
  int countPC;
  int countPlayer;
  time_t Playtime;
  struct Node *prev;
  struct Node *next;
};

struct List {
  char *namePC;
  char *namePl;
  struct Node *head;
  struct Node *tail;
};

struct List *init(char *namePlayer, char *PC) {
  struct List *tmp;
  tmp = (struct List *)malloc(sizeof(struct List));
  tmp->namePC = PC;
  tmp->namePl = namePlayer;
  tmp->head = tmp->tail = NULL;
  return tmp;
}

void add_node(struct List *list, int scorePlayer, int scorePC) {

  static struct Node *tmp;
  tmp = (struct Node *)malloc(sizeof(struct Node));

  if (list->head == NULL) {
    tmp->countPC = scorePC;
    tmp->countPlayer = scorePlayer;
    tmp->Playtime = time(NULL);

    tmp->next = NULL;
    tmp->prev = NULL;
    list->head = list->tail = tmp;
  } else {
    tmp->countPC = scorePC;
    tmp->countPlayer = scorePlayer;
    tmp->Playtime = time(NULL);

    tmp->prev = list->tail;
    tmp->next = NULL;
    list->tail->next = tmp;
    list->tail = tmp;
  }
}

void print_list(struct List *list) {
  struct Node *tmp = list->head;
  do {
    printf("%s - %d\n", list->namePl, tmp->countPlayer);
    printf("%s - %d\n", list->namePC, tmp->countPC);
    printf("%s\n", ctime(&tmp->Playtime));
    printf("--------------------\n");
    tmp = tmp->next;
  } while (tmp->next != NULL);
}

void delete_list(struct List *list) {
  struct Node *tmp = list->head;
  list->head = tmp->next;
  do {
    free(tmp);
    tmp = list->head;
    list->head = tmp->next;
  } while (list->head != NULL);
}
