#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "List.h"

#define MinNumCube 1
#define MaxNumCube 6

int main(int argc, char *argv[]) {

  srand(time(NULL));
  struct List *list;
  char *player = "Dmitriy";
  char *pc = "PC";
  int pointPC, pointPlayer, min = 0, max = 0;

  if (argc == 1 || argc > 2) {
    printf("you must specify a parameter or no more than one param eter\n");
    return -2;
  } else {
    if (strcmp(argv[1], "1") == 0) {
      min = MinNumCube;
      max = MaxNumCube;
    } else if (strcmp(argv[1], "2") == 0) {
      min = MinNumCube << 1;
      max = MaxNumCube << 1;
    } else {
      printf("enter the number of cubes 1 or 2\n");
      return -1;
    }
  }
  list = init(player, pc);

  for (int i = 0; i < 10; i++) {
    pointPC = rand() % (max - min + 1) + min;
    pointPlayer = rand() % (max - min + 1) + min;
    add_node(list, pointPlayer, pointPC);
  }

  print_list(list);

  delete_list(list);
  free(list);
  return 0;
}
