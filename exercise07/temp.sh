#!/bin/bash

echo 26 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio26/direction

count=0
var3=true

while [ true  ]
do 

read_value=`cat /sys/class/gpio/gpio26/value`

sec=`i2cget -y 1 0x68 0x00`
min=`i2cget -y 1 0x68 0x01`
hour=`i2cget -y 1 0x68 0x02`

high=`i2cget -y 1 0x68 0x11`
low=`i2cget -y 1 0x68 0x12`

var2=$(( (( high << 8 ) | ( low & 0xC0 )) >> 6 ))
deg=$(( ( var2 * 100  ) / 4 ))

if [[ $read_value == "1" && $var3 == true  ]] ; then

echo h:${hour:2:${#hour}} min:${min:2:${#min}} sec:${sec:2:${#sec}}
echo temp:`printf %.2f "$(( deg ))e-2"`

var3=false

elif [[ $count == "7" ]] ; then

echo 26 > /sys/class/gpio/unexport
break 

elif [[ $read_value == "1" ]] ; then 

sleep .1
count=$((count+1))

elif [[ $read_value == "0" ]] ; then

count=0
var3=true

fi
done
