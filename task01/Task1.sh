#!/bin/bash

#echo "Choose display mode"
#echo "1: Full"
#echo "2: Short"

mode=$1
path=$2
arr=()

function hlp {
clear
echo "help"
read
}

function fill_array {
for i in ${!arr[@]}; do

  str=${arr[$i]}
  arr[$i]=$i-$str
  echo ${arr[$i]}

done
 echo
}

while [ true ]
do

if [[ $path != "" ]] ; then
    cd $path
    path=""
fi

if [[ $mode == "1" ]] ; then
    arr=($(ls -a))
     fill_array

elif [[ $mode == "2" ]] ; then
    arr=($(ls))
    fill_array

elif [[ $mode == "3" ]] ; then
    #arr=($(ls */ ))
    #fill_array
    ls -d */
elif [[ $mode == "-h" ]] ; then
    hlp

exit 0
    fi

echo "1: change the directory"
echo "2: copy file"
echo "3: move/rename"
echo "4: delete file"
echo "5: change mod view or view help"
echo "0: exit"
read choose
case "$choose" in

"1")
    echo "Enter directory for change"
    read var
    cd $var
;;
"2")
    echo "select the file to copy"
    read i
    str=${arr[$i]}
    echo "new file or directory"
    read var2
    cp ${str:2:${#str}} $var2
;;
"3")
    echo "select the file to move/rename"
    read i
    str=${arr[$i]}
    echo "new name or directory"
    read var2
    mv ${str:2:${#str}} $var2
;;
"4")
    echo "select the file to delete"
    read var
    for i in $var; do
    str=${arr[$i]}
    rm ${str:2:${#str}}
    done
;;
"5")
read var
if [[ $var == "h" ]] ; then
        hlp

else
mode=$var
fi
;;
"0")
break
;;
esac
done
