#!/bin/bash

while [ true ] 
do
#echo "Choose directory"
read choose
if [[ "$choose" =~ .*"/".* ]] ; then
  v=$choose
choose=`pwd`
choose+="$v"

for (( i=0; $i<${#choose}; i=$(($i+1)) ))
        do
        var="${choose:$i:1}"
        if [ "$var" !=  " " ] ; then
        continue
        else
        break
        fi
        done
        path=${choose:0:$i}


#else
#v=$choose
#choose=`pwd`
#choose+="$v"
fi



 if [[ "$choose" =~ .*"-r".* && "$choose" =~ .*"-y".* || "$choose" =~ .*"--recursive".* &&  "$choose" =~ .*"--yes".* ]] ; then
        VAR1="*"
        var3="$path$VAR1"
        rm -r  $var3 #[-,_,~,a-zA-Z]*.tmp

elif [[ "$choose" =~ .*"-r".* || "$choose" =~ .*"--recursive".* ]] ; then
        VAR1="*"
        var3="$path$VAR1"
        rm -ri $var3 #[-,_,~,a-zA-Z]*.tmp
        
fi       
        
if [[ "$choose" =~ .*"-t".* || "$choose" =~ .*"--test".* ]] ; then

        cd $path
        realpath  --  [-,_,~,a-zA-Z]*.tmp
fi

if [[ "$choose" =~ .*"-h".* ||  "$choose" =~ .*"--help".*  ]] ; then
echo "    -r"
echo "deleting is performed in all nested directories"
echo "empty directories are deleted after recursive deletion (excluding the initial one)"
echo "     -t"
echo "paths to files in the directory are shown but not deleted"
echo "     -m"
echo "deleting by mask"
echo "      -y"
echo "deleting without confirmation"
fi
done

