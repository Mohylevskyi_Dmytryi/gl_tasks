#!/bin/bash
dmesg -C
insmod sysfs.ko
insmod procfs.ko
str="hello world"

echo "test str: $str"
echo "mode 0"
echo $str > /proc/example/buffer
var=`cat /proc/example/buffer`
if [[ $var == "hello world" ]] ; then
    echo "test for mod 0 pass"
    echo $var
fi

echo "mode 1"
echo "1" > /sys/class/x-class/xxx
echo $str > /proc/example/buffer
var=`cat /proc/example/buffer`
if [[ $var == "olleh dlrow" ]] ; then
    echo "test for mod 1 pass"
    echo $var
fi

echo "mode 2"
echo "2" > /sys/class/x-class/xxx
echo "hello world" > /proc/example/buffer
var=`cat /proc/example/buffer`
if [[ $var == "HELLO WORLD" ]] ; then
    echo "test for mod 2 pass"
    echo $var
fi

rmmod procfs.ko
rmmod sysfs.ko
