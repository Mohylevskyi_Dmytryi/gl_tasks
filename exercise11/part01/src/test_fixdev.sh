#!/bin/bash

 mknod node_ c 200 0
 mknod node_1 c 200 1
 mknod node_2 c 200 2
 mknod node_3 c 200 3
 mknod node_4 c 200 4

dmesg -C
 insmod fixdev.ko major=200
 cat node_
 cat node_1
 cat node_2
 cat node_3
 cat node_4

rm -f node_*
 rmmod fixdev

insmod fixdev.ko major=0
rmmod fixdev
dmesg | tail -20
