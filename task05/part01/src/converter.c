#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("xone");
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.01");

static int euro = 0;
module_param(euro,int,0660);
static int grn = 0;
module_param(grn,int,0660);

static int __init ex01_init(void)
{
    int resLeft, resRight, inLeft, inRight, inLeft2, inRight2;
    char str[20];

	int curs1euro_grn = 3195;

	inLeft = euro / (100);
	inRight = euro % (100);

	inLeft2 = grn / (100);
	inRight2 = grn % (100);

	int tmp = euro * curs1euro_grn;

	resLeft = tmp / (10 * 10 * 10 * 10);
	resRight = (tmp % (10 * 10 * 10 * 10)) / 100;

    int j = sprintf(str, "%d%c", grn / curs1euro_grn, '.');

	grn = (grn % curs1euro_grn) * 10;
	str[j] = (grn / curs1euro_grn) + 48;
	j++;

	grn = (grn % curs1euro_grn) * 10;
	int a = grn / curs1euro_grn;
	tmp = (grn % curs1euro_grn) * 10 / curs1euro_grn;

    if (tmp >= 5 && a != 9)
		a++;

	str[j] = a + 48;
	str[j + 1] = '\0';

	printk(KERN_INFO "%d.%d euro =  %d.%d grn\n", inLeft, inRight, resLeft, resRight);
	printk(KERN_INFO "%d.%d grn =  %s euro\n", inLeft2, inRight2, str);

	return -1;
}

module_init(ex01_init);

